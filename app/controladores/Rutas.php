<?php namespace app\controladores; 
    

    class Rutas{

        /**
         * esta funcion valida la informacion de la url para ver que no venga vacia
         *
         *  con tiene un  if que retorna un valor dependiendo del contenido de la variable
         * 
         * @param string $param1 resive la url 
         * @return string retorna un valor luego de un comprobacion si la ulr contiene informacion o no
         * 
         * provemos como se esto
         */
        function  comprobarUrl($param1){

            if(isset($param1)){ // ? se comprueba si existe la url o contiene un valor

                return $param1; // TODO retorna el valor del parametro 

            }else{

                return $param1 = null; // TODO retorna el valor del parametro como null

            }
        }

        function comprobarRuta($param1){

            $param1= strtolower($param1); // convertimos todas las letras en minusculas
            // $param1= ucfirst($param1); // convertimos la primera letra en mayuscula
            $carpeta = dirname(__FILE__);  //  guardamos en una variable la ruta del archivo
            $carpeta = str_replace("\\", "/", $carpeta); //  remplazamos \\ por / para no generar conflictos
            $existecarpeta = file_exists($carpeta."/../../vista/public/template/".$param1.".php"); // comprabamos que exita el archivo que controla la vista y creamos una nueva variable 
            $rutafinal= $carpeta."/../../vista/public/template/principal.php"; // guardamos en una nueva variable la ruta final del archivo
            
            if($param1 == null){ //validamos si el parametro $url contiene informacion o no
                    
                include_once $carpeta."/InicioControlador.php"; // si no tiene informacion lo enviamos a la pagina de inicio

            }else if($existecarpeta){

                if($param1!="principal"){

                    //? include_once $rutafinal; // si contiene una vista valida lo direcionamos a ella
                    include_once("vista/public/template/principal.php");

                }else{
                    include_once $carpeta."/404Controlador.php";
                }
                
            }else{
                include_once $carpeta."/404Controlador.php";
            }
        }
    }