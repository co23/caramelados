<?php

    spl_autoload_register(function($clase){

        $ruta = str_replace("\\","/",$clase).'.php' ;

        if(!file_exists($ruta)){
            throw new Exception("error al cargar la clase". $ruta );
        }
    
        require_once($ruta);

    });


?>